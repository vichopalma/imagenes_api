import React from 'react';
import LinearProgress from '@material-ui/core/LinearProgress';

const ImageScore = (props) => {
  //props.ups is the number of upvotes
  //props.downs is the number of downvotes

  const { ups, downs } = props;

  //const upsPercent = `${100*(ups / (ups+downs))}%`;
  //const downsPercent = `${100*(downs / (ups+downs))}%`;

  iUpsPercent = 100*(ups / (ups+downs));
  idownsPercent = 100*(downs / (ups+downs));

  const styles = {
  root: {
    flexGrow: 1,
  },
};

  return(
    <div>
        Ups/down votes
        <div>
          <LinearProgress variant="determinate" value={iUpsPercent} />
          <br />
          <LinearProgress color="secondary" variant="determinate" value={idownsPercent} />
        </div>
    </div>

  );

};

//Export our component
export default ImageScore;
