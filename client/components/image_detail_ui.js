import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import ListItemText from '@material-ui/core/ListItemText';

import ImageScore from './image_score';

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    marginTop: theme.spacing.unit * 3,
  }),
});

function ImageDetailUi(props) {
  const { classes } = props;
  return (

      <Paper className={classes.root} elevation={4}>
        <div>
            <img src={props.picture.link} />
        </div>
        <div>
          <h4>
            <ListItemText primary= {props.picture.title} />
          </h4>
          <p>
            {props.picture.description}
          </p>
          <div>
            <ImageScore ups={props.picture.ups} downs={props.picture.downs}/>
          </div>
        </div>
      </Paper>

  );
};

ImageDetailUi.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ImageDetailUi);
