//Create our image list component
//Import React
import React from 'react';
import ImageDetail from './image_detail';


const IMAGES = [
  { tittle: 'vicho', link: 'https://pbs.twimg.com/profile_images/905918800230580224/alfJrfFx_400x400.jpg' },
  { tittle: 'rick', link: 'https://pbs.twimg.com/profile_images/870105716702445570/NGZ7ZMjw_400x400.jpg' },
  { tittle: 'madara', link: 'https://steamusercontent-a.akamaihd.net/ugc/868491307794689640/0A92867F7743E97A7543B888351B704B0AD1AB51/' }
];

//Create our component
const ImageList = () => {
  const RenderedImages = IMAGES.map(function(image) {
    return <ImageDetail picture = {image} />
  });

  return (
    <ul>
      {RenderedImages}
    </ul>
  );
};


//Export our component
export default ImageList;
