//Create our image list component
//Import React
import React from 'react';

import ImageDetailUi from './image_detail_ui';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItemText from '@material-ui/core/ListItemText';

//const IMAGES = [
//  { tittle: 'Daredevil', link: 'https://pbs.twimg.com/profile_images/905918800230580224/alfJrfFx_400x400.jpg' },
//  { tittle: 'rick', link: 'https://pbs.twimg.com/profile_images/870105716702445570/NGZ7ZMjw_400x400.jpg' },
//  { tittle: 'madara', link: 'https://steamusercontent-a.akamaihd.net/ugc/868491307794689640/0A92867F7743E97A7543B888351B704B0AD1AB51/' }
//];
const videoExtension = 'video/mp4';
const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.purple,
  },
});

//Create our component
function ImageListUi(props) {
//filtro para no enviar objetos tipo "album"(array de imagenes) para solo entregar objetos tipo imagen
const validImages = props.images.filter(image => !image.is_album && image.type != videoExtension);

//busca las imagenes en el filtro realizado
  const RenderedImages = validImages.map( image => {
    return <ImageDetailUi key={image.id} picture = {image} />
  });

const { classes } = props;

  return (
    <div className={classes.root}>
      <List component="nav">

        {RenderedImages}

      </List>
    </div>
  );
};

ImageListUi.propTypes = {
  classes: PropTypes.object.isRequired,
};

//Export our component
export default withStyles(styles)(ImageListUi);
