import React from 'react';

const ImageDetail = (props) => {
  // props.picture => this is the picture object
  //props.picture.tittle
  //props.picture.link

  return(
    <li>
      <img src={props.picture.link} />
      {props.picture.title}
    </li>
  );
}

export default ImageDetail;
