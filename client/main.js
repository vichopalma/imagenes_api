//console.log("Hola Mundo!");
//Cualquier JS aqui corre automaticamente por nosotros

//Import the react library
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import ImageList from './components/image_list';
import ImageDetailUi from './components/image_detail_ui';
import ImageListUi from './components/image_list_ui';

import axios from 'axios';

//Create a component
class App extends Component {

  constructor(props) {
    super(props);

    this.state = { images: [] };
  }


//is a metod where if we decide to define it will be automatically called by react
//whenever this component is about to render to the DOM
//fantastic place to load data!!!
componentWillMount(){
  console.log('App is about to render');
  //Entrega el token de acceso (autorizacion/llave)
  //axios.get('https://api.imgur.com/oauth2/authorize?response_type=token&client_id=e3b0cbf6d2b1347')
  //  .then( response => console.log(response) );

  axios.get('https://api.imgur.com/3/gallery/top/top', {
  headers: {'Authorization': 'Bearer 098748b3488c00b2ca953d609b26c05b660cc311'},
  })
    .then( response => this.setState( { images: response.data.data } ) );
    //NEVER DO THIS-
    //this.state.images = [ {} , {} ];
}

//a ImageListUi le pasamos un props llamado images con los datos provenientes de this.state.images
//como this.state.images es un array, el componente ImageListUi puede tratarlo de la misma forma.
  render(){
    console.log(this.state.images);
    return (
      <div>
        <ImageListUi images={this.state.images}/>
      </div>
    );
  }
};

//Render this component to the screen
Meteor.startup(() => {
  ReactDOM.render(<App />, document.querySelector('.container'));
});
